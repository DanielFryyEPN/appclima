//
//  ViewController.swift
//  AppClima
//
//  Created by Daniel Freire on 25/10/17.
//  Copyright © 2017 Daniel Freire. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var usuarioTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // MARK:- ViewCOntroler Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        usuarioTextField.text = ""
        passwordTextField.text = ""
        usuarioTextField.becomeFirstResponder()
    }
    
    // MARK:- Actions
    @IBAction func entrarButtonPressed(_ sender: UIButton) {
        let user = usuarioTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        switch(user, password) {
        case ("daniel", "123456"):
            performSegue(withIdentifier: "ciudadSegue", sender: self)
        case("daniel", _):
            print("contraseña incorrecta")
        default:
            print("usuario y contraseña incorrectas")
        }
    }
    
    @IBAction func invitadoButtonPressed(_ sender: UIButton) {
    }
}

