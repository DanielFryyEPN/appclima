//
//  CiudadViewController.swift
//  AppClima
//
//  Created by Daniel Freire on 31/10/17.
//  Copyright © 2017 Daniel Freire. All rights reserved.
//

import UIKit

class CiudadViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var weatherLabel: UILabel!
    
    // MARK:- ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:- Actions
    @IBAction func consultarButtonPressed(_ sender: UIButton) {
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text ?? "quito")&appid=be5f29ecd9f9aa1d4f0cb1a555a07012"
        
        let url = URL(string: urlStr)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            if let _ = err {
                return
            }
            
            do {
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                let weatherDict = weatherJson as! NSDictionary
                guard let weatherKey = weatherDict["weather"] as? NSArray else {
                    DispatchQueue.main.async {
                        self.weatherLabel.text = "Ciudad no valida"
                    }
                    return
                }
                let weather = weatherKey[0] as! NSDictionary
                DispatchQueue.main.async {
                    self.weatherLabel.text = "\(weather["description"] ?? "Error")"
                }
            } catch {
                print("Error al generar el Json")
            }
        }
        
        task.resume()
    }
}
